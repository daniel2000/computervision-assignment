import cv2
import numpy as np

#https://github.com/dylanseychell/COTSDataset/blob/master/Part%202%20-%20Multiple%20Objects/wash_no/2_colour.jpeg
S1 = cv2.imread("testimages/1_colour.jpeg")
#https://github.com/dylanseychell/COTSDataset/blob/master/Part%202%20-%20Multiple%20Objects/wash_no/1_colour.jpeg
S2 = cv2.imread("testimages/2_colour.jpeg")

# New Background images
bg1 = cv2.imread("bgimages/bench.jpg")
bg2 = cv2.imread("bgimages/popup.jpg")
bg3 = cv2.imread("bgimages/green.jpg")
bg4 = cv2.imread("bgimages/avatar.jpg")

# List to store all 4 backgrounds to replace the green screen
bg_list = [bg1, bg2, bg3, bg4]


# Function to apply the Closing morphological operation to the input image
def Closing(img):
    kernel_type = cv2.MORPH_RECT
    kernel_size = 4
    kernel_SE = cv2.getStructuringElement(kernel_type, (2 * kernel_size + 1, 2 * kernel_size + 1), (kernel_size, kernel_size))

    return cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel_SE)


# Function to remove the green background from the input image and return the result
def RemoveGreen(img):

    # Define Green Threshold
    lower_green = np.array([0, 24, 0])
    upper_green = np.array([130, 255, 120])

    # Mask which captures the green background from the input image
    mask = cv2.inRange(img, lower_green, upper_green)

    # Invert mask so what white pixels represent objects whilst black represent the background
    inverted_mask = 255 - mask

    # Show inverted mask generated for testing purposes
    cv2.imshow("Green Screen Mask", inverted_mask)

    # Dilate mask to fill objects using the closing morphological operation
    morph_mask = Closing(inverted_mask)

    # Show closed mask for testing purposes
    cv2.imshow("Mask after Closing Morphological Operator", morph_mask)

    # Copy of image
    masked_img = img.copy()

    # Use Morphed mask to set green background pixels to black pixels, thus removing the background
    masked_img[morph_mask == 0] = [0, 0, 0]

    return masked_img


# When this section is run the function should display an image without green background
imgNoBgOne = RemoveGreen(S1)
cv2.imshow("No Green Background - S1", imgNoBgOne)
cv2.waitKey()

cv2.destroyAllWindows()

imgNoBgTwo = RemoveGreen(S2)
cv2.imshow("No Green Background - S2", imgNoBgTwo)
cv2.waitKey()

cv2.destroyAllWindows()


# Function to update the image objects with a new background and return the result
def NewBackground(imgNoBg, newBackground):

    # Define grayscale mask
    mask = cv2.cvtColor(imgNoBg, cv2.COLOR_BGR2GRAY)

    # Get image height and width
    img_height = imgNoBg.shape[0]
    img_width = imgNoBg.shape[1]

    # Crop Background image for it to have the same size as the input image
    crop_bg = newBackground[0:img_height, 0:img_width]

    # Block part of the background image where mask has objects
    crop_bg[mask != 0] = [0, 0, 0]

    # Combine cropped background with image with no background
    newBackgroundImg = crop_bg + imgNoBg

    return newBackgroundImg

# Display images with all new backgrounds within bg_list
for i in range(len(bg_list)):
    newBackgroundImgOne = NewBackground(imgNoBgOne, bg_list[i])
    cv2.imshow("Image with new background {} - S1".format(i+1), newBackgroundImgOne)
    cv2.waitKey()

cv2.destroyAllWindows()

for i in range(len(bg_list)):
    newBackgroundImgTwo = NewBackground(imgNoBgTwo, bg_list[i])
    cv2.imshow("Image with new background {} - S2".format(i+1), newBackgroundImgTwo)
    cv2.waitKey()

cv2.destroyAllWindows()