import requests
import cv2
import base64
import io
from imageio import imread
import numpy as np
import urllib.request as urllib

def convertBase64ToCVImage(base64img):
    """
    img = imread(io.BytesIO(base64.b64decode(base64img)))
    return cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    """
    im_bytes = base64.b64decode(base64img)
    im_arr = np.frombuffer(im_bytes, dtype="uint8")  # im_arr is one-dim Numpy array
    return cv2.imdecode(im_arr, flags=cv2.IMREAD_COLOR)


def convertCVImageToBase64(cvImg):
    _, im_arr = cv2.imencode('.jpg', cvImg)  # im_arr: image in Numpy one-dim array format.
    im_bytes = im_arr.tobytes()
    return base64.b64encode(im_bytes)

def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    resp = urllib.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    # return the image
    return image

S1 = url_to_image("https://i.ibb.co/vmSNwt0/1-colour.jpg")
S2 = url_to_image("https://i.ibb.co/yyYJJtV/2-colour.jpg")
ObjectMask= url_to_image("https://i.ibb.co/6yX9Wvw/wash-no-3-colour-mask-5-mask.png")



cv2.imshow("DS",ObjectMask)
cv2.waitKey(0)

S1Base64 = convertCVImageToBase64(S1)
S2Base64 = convertCVImageToBase64(S2)
ObjectMaskBase64 = convertCVImageToBase64(ObjectMask)
ss11111 = convertBase64ToCVImage(S1Base64)
print("ds")
"""
with open("testimages/wash_no_3_colour_mask_5_mask.png", "rb") as image_file1:
    ObjectMaskBase64 = base64.b64encode(image_file1.read())

with open("testimages/2_colour.jpeg", "rb") as image_file2:
    S2Base64 = base64.b64encode(image_file2.read())

with open("testimages/1_colour.jpeg", "rb") as image_file3:
    S1Base64 = base64.b64encode(image_file3.read())
"""

ApplyMorphologyParameters = {'img':ObjectMaskBase64,'morphIndex':0}
ApplyMorphologyResult = requests.post("http://127.0.0.1:5000/cvassignment/applyMorphology/",data=ApplyMorphologyParameters)

ObjectMaskBase64 = ApplyMorphologyResult.content

ObjectMaskBase64CVImg = convertBase64ToCVImage(ObjectMaskBase64)
cv2.imshow("Object Mask After Morphology",ObjectMaskBase64CVImg)
cv2.waitKey(0)

extractObjectParameters ={'S2':S2Base64,'ObjectMask':ObjectMaskBase64}
# Make a get request with the parameters.
ExtractedObject = requests.post("http://127.0.0.1:5000/cvassignment/extractObject/",data=extractObjectParameters)

extractedObjectCVImg = convertBase64ToCVImage(ExtractedObject.content)
cv2.imshow("Extracted Object",extractedObjectCVImg)
cv2.waitKey(0)


# Set up the parameters we want to pass to the API.
# This is the latitude and longitude of New York City.
ApplyFilterParameters ={'ExtractedObject':ExtractedObject.content,'FilterIndex':0}
FilteredExObjectOne = requests.post("http://127.0.0.1:5000/cvassignment/applyFilter",data=ApplyFilterParameters)


FilteredExObjectOneCVImg = convertBase64ToCVImage(FilteredExObjectOne.content)
cv2.imshow("Filtered Object - 1",FilteredExObjectOneCVImg)
cv2.waitKey(0)


ApplyMorphologyParameters = {'img':ObjectMaskBase64,'morphIndex':0}
ApplyMorphologyResult = requests.post("http://127.0.0.1:5000/cvassignment/applyMorphology",data=ApplyMorphologyParameters)

ObjectMaskBase64 = ApplyMorphologyResult.content

ObjectBlenderParameters = {'S1':S1Base64,'FilteredExObject':FilteredExObjectOne.content,'ObjectMask':ObjectMaskBase64}
BlendingResultOne = requests.post("http://127.0.0.1:5000/cvassignment/objectBlender",data=ObjectBlenderParameters)

BlendingResultOneCVImg = convertBase64ToCVImage(BlendingResultOne.content)
cv2.imshow("Blending Result - 1",BlendingResultOneCVImg)
cv2.waitKey(0)


CompareResultsParameters = {'BlendingResult':BlendingResultOne.content,'S2':S2Base64,'metric':1}
results = requests.post("http://127.0.0.1:5000/cvassignment/compareResult",data=CompareResultsParameters)

print(float(results.content.decode()))

# Print the content of the response (the data the server returned)
#cv2.imshow("test" ,response.content)
#cv2.waitKey(0)
# This gets the same data as the command aboveresponse = requests.get("http://api.open-notify.org/iss-pass.json?lat=40.71&lon=-74")
#print((response.content).decode('base64'))
#file  = io.BytesIO(base64.b64decode((response.content)))
#img  = cv2.imread(file)



#img2 = convertBase64ToCVImage(response.content)


#cv2.imshow("Re",img2)
#cv2.waitKey(0)