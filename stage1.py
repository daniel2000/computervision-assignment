import cv2
import numpy as np

# IDE INSTALLATION: pip install scikit-image
# ANACONDA INSTALLATION: conda install -c conda-forge scikit-image
# Reference: https://scikit-image.org/
from skimage.metrics import structural_similarity as ssim

#https://github.com/dylanseychell/COTSDataset/blob/master/Part%202%20-%20Multiple%20Objects/wash_no/2_colour.jpeg
S1 = cv2.imread("testimages/1_colour.jpeg")
#https://github.com/dylanseychell/COTSDataset/blob/master/Part%202%20-%20Multiple%20Objects/wash_no/1_colour.jpeg
S2 = cv2.imread("testimages/2_colour.jpeg")
#https://github.com/dylanseychell/COTSDataset/blob/master/Part%202%20-%20Multiple%20Objects/wash_no/masks/wash_no_3_colour_mask_5_mask.png
ObjectMask = cv2.imread("testimages/wash_no_3_colour_mask_5_mask.png")


# Function to apply specific morphological operations to the object mask
# This is done so as to extract the object as best as possible with a minimum amount of green pixel outliers
def applyMorphology(img, morphIndex):

    kernel_type = cv2.MORPH_RECT

    # Morphological Operation done before object extraction
    if morphIndex == 0:
        dilation_size = 3
        dilation_SE = cv2.getStructuringElement(kernel_type, (2 * dilation_size + 1, 2 * dilation_size + 1),(dilation_size, dilation_size))
        morph_img = cv2.dilate(img, dilation_SE)

        return morph_img

    # Morphological Operation done before object blending
    elif morphIndex == 1:
        erosion_size = 1
        erosion_SE = cv2.getStructuringElement(kernel_type, (2 * erosion_size + 1, 2 * erosion_size + 1),(erosion_size, erosion_size))
        morph_img = cv2.erode(img, erosion_SE)

        return morph_img


# Function to carry out object extraction
def ExtractObject(S2, ObjectMask):

    # Convert object mask to single channel image
    ObjectMask = cv2.cvtColor(ObjectMask, cv2.COLOR_BGR2GRAY)

    # Extract object from S2 using morphed mask
    ExtractedObject = cv2.bitwise_and(S2, S2, mask=ObjectMask)

    return ExtractedObject


# Apply morphological operations to mask before object extraction
ObjectMask = applyMorphology(ObjectMask, 0)

# Extract Object from S2
ExtractedObject = ExtractObject(S2, ObjectMask)
cv2.imshow("Extracted Object", ExtractedObject)
cv2.waitKey()

cv2.destroyAllWindows()


# Function to apply a convolution filter to the extracted object
def ApplyFilter(ExtractedObject, FilterIndex):

    FilteredExObject = None

    # No Convolution filter
    if FilterIndex == 0:
        FilteredExObject = ExtractedObject

    # Bilinear Filtering
    elif FilterIndex == 1:

        kernel = np.array([
            [1, 2, 1],
            [2, 4, 2],
            [1, 2, 1]
        ])/16

        FilteredExObject = cv2.filter2D(ExtractedObject, -1, kernel)

    # Gaussian Filtering
    elif FilterIndex == 2:

        kernel = np.array([
            [1, 4, 6, 4, 1],
            [4, 16, 24, 16, 4],
            [6, 24, 36, 24, 6],
            [4, 16, 24, 16, 4],
            [1, 4, 6, 4, 1],
        ])/256

        FilteredExObject = cv2.filter2D(ExtractedObject, -1, kernel)

    # Bilateral Filtering
    elif FilterIndex == 3:
        FilteredExObject = cv2.bilateralFilter(ExtractedObject,9,75,75)

    else:
        print("Error: No kernel exists for FilterIndex: {}".format(FilterIndex))
        print("Make sure index range is within 0-3 limits.")
        exit()

    return FilteredExObject


# When this section is run the function should return the object as it is marked at index 0
FilteredExObjectOne = ApplyFilter(ExtractedObject, 0)
cv2.imshow("Filtered Object - 0", FilteredExObjectOne)
cv2.waitKey()

# When this section is run the function should return the filtered object marked at index 1
FilteredExObjectTwo = ApplyFilter(ExtractedObject, 1)
cv2.imshow("Filtered Object - 1", FilteredExObjectTwo)
cv2.waitKey()

# When this section is run the function should return the filtered object marked at index 2
FilteredExObjectThree = ApplyFilter(ExtractedObject, 2)
cv2.imshow("Filtered Object - 2", FilteredExObjectThree)
cv2.waitKey()

# When this section is run the function should return the filtered object marked at index 3
FilteredExObjectFour = ApplyFilter(ExtractedObject, 3)
cv2.imshow("Filtered Object - 3", FilteredExObjectFour)
cv2.waitKey()

cv2.destroyAllWindows()

def ObjectBlender(S1, FilteredExObject):

    # Convert S1 and Filtered Extract to float types
    S1 = S1.astype(float)
    FilteredExObject = FilteredExObject.astype(float)

    # Create the alpha mask using the ObjectMask and normalizing the intensity between 0 and 1
    alpha = ObjectMask.astype(float)/255

    # Beta is the inverse of the alpha mask
    beta = 1 - alpha

    # Apply alpha mask to the Extract since only the object is required
    FilteredExObject = cv2.multiply(alpha, FilteredExObject)

    # Apply beta mask to S1 since everything is required from it except for the pixel to be overridden by the extract
    S1 = cv2.multiply(beta, S1)

    # Done to represent the following simple addition blending equation: g(x)=(1−α)f0(x)+αf1(x)
    BlendingResult = cv2.add(S1, FilteredExObject)

    # Convert BlendingResult into a uint8 format that works with OpenCV (Instead of 64bit floating point)
    BlendingResult = BlendingResult.astype(np.uint8)

    return BlendingResult


# Apply morphological operation to mask before object blending
# This was done since after applying the extracted object to different filters, some filters blurred black pixels on to
# the region denoted by the object mask. The result of this lead to an outline of the extracted object to be shown,
# which was not ideal. Thus, erosion of size 1 was applied to the object mask so that the outline is completely removed.
ObjectMask = applyMorphology(ObjectMask, 1)

# When this section is run the function should return the blended image for FilteredExObjectOne
BlendingResultOne = ObjectBlender(S1, FilteredExObjectOne)
cv2.imshow("Blending Result - 1", BlendingResultOne)
cv2.waitKey()

# When this section is run the function should return the blended image for FilteredExObjectTwo
BlendingResultTwo = ObjectBlender(S1, FilteredExObjectTwo)
cv2.imshow("Blending Result - 2", BlendingResultTwo)
cv2.waitKey()

# When this section is run the function should return the blended image for FilteredExObjectThree
BlendingResultThree = ObjectBlender(S1, FilteredExObjectThree)
cv2.imshow("Blending Result - 3", BlendingResultThree)
cv2.waitKey()

# When this section is run the function should return the blended image for FilteredExObjectFour
BlendingResultFour = ObjectBlender(S1, FilteredExObjectFour)
cv2.imshow("Blending Result - 4", BlendingResultFour)
cv2.waitKey()

cv2.destroyAllWindows()


def CompareResult(BlendingResult, S2, metric):

    # Error Value declaration
    eV = None

    # Convert BlendingResult and S2 to grayscale for single channel error calculations
    BlendingResult = cv2.cvtColor(BlendingResult, cv2.COLOR_BGR2GRAY)
    S2 = cv2.cvtColor(S2, cv2.COLOR_BGR2GRAY)

    # Convert BlendingResult and S2 to float types
    BlendingResult = BlendingResult.astype(float)
    S2 = S2.astype(float)

    # SSD Error Metric - Sum of Squared Differences
    # Value of 0 indicates perfect similarity
    # A value greater implies less similarity
    if metric == 0:

        # SSD = sum of the squared difference between the two images
        eV = np.sum((BlendingResult - S2)**2)

    # MSE Error Metric - Mean Squared Error
    # Value of 0 indicates perfect similarity
    # A value greater than one implies less similarity
    elif metric == 1:

        # MSE = mean of sum of the squared difference between the two images
        eV = np.sum((BlendingResult - S2) ** 2)

        # divide sum of squares by total number of pixels in the image (both images have same dimension)
        eV /= float(BlendingResult.shape[0] * BlendingResult.shape[1])

    # SSIM Error Metric - Structural Similarity Index
    # Value of 1 indicates perfect similarity
    # Value can vary between -1 and 1. The closer it is to -1 the less similarity of the images
    elif metric == 2:

        # Compute SSIM using scikit-image SSIM definition
        eV = ssim(BlendingResult, S2)

    else:
        print("Error: No metric exists for index: {}".format(metric))
        print("Make sure index is either 0 or 1")
        exit()

    return eV


# When this section is run the function should return the error Value
print("\nError values for blending result - 1")
for x in range(3):
    eV = CompareResult(BlendingResultOne, S2, x)

    if x == 0:
        print("SSD Metric  : {:.2f}".format(eV))
    elif x == 1:
        print("MSE Metric  : {:.2f}".format(eV))
    elif x == 2:
        print("SSIM Metric : {:.4f}".format(eV))


print("\nError values for blending result - 2")
for x in range(3):
    eV = CompareResult(BlendingResultTwo, S2, x)

    if x == 0:
        print("SSD Metric  : {:.2f}".format(eV))
    elif x == 1:
        print("MSE Metric  : {:.2f}".format(eV))
    elif x == 2:
        print("SSIM Metric : {:.4f}".format(eV))


print("\nError values for blending result - 3")
for x in range(3):
    eV = CompareResult(BlendingResultThree, S2, x)

    if x == 0:
        print("SSD Metric  : {:.2f}".format(eV))
    elif x == 1:
        print("MSE Metric  : {:.2f}".format(eV))
    elif x == 2:
        print("SSIM Metric : {:.4f}".format(eV))


print("\nError values for blending result - 4")
for x in range(3):
    eV = CompareResult(BlendingResultFour, S2, x)

    if x == 0:
        print("SSD Metric  : {:.2f}".format(eV))
    elif x == 1:
        print("MSE Metric  : {:.2f}".format(eV))
    elif x == 2:
        print("SSIM Metric : {:.4f}".format(eV))
